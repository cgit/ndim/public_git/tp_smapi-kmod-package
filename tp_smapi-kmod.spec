# (un)define the next line to either build for the newest or all current kernels
#define buildforkernels newest
%define buildforkernels akmod

Name:           tp_smapi-kmod
Version:        0.40
Release:        3%{?dist}
Summary:        ThinkPad System Management API - kernel driver

Group:          System Environment/Kernel
License:        GPLv2+
URL:            http://tpctl.sourceforge.net/
Source0:        http://prdownloads.sourceforge.net/tpctl/tp_smapi-%{version}.tgz
Source11:       tp_smapi-kmodtool-excludekernel-filterfile
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

# There are no ThinkPads with architectures other than IA32 or x86_64
ExclusiveArch:  %{ix86} x86_64

# The debuginfo package would be empty anyway, so skip it.
#define debug_package %{nil}

# Whether to build with HDAPS support or not
#define with_hdaps 0
%define with_hdaps 1

%define repo rpmfusion

# get the needed BuildRequires (in parts depending on what we build for)
BuildRequires:  %{_bindir}/kmodtool
BuildRequires:  akmods
%{!?kernels:BuildRequires: buildsys-build-%{repo}-kerneldevpkgs-%{?buildforkernels:%{buildforkernels}}%{!?buildforkernels:current}-%{_target_cpu} }
# kmodtool does its magic here
%{expand:%(kmodtool --target %{_target_cpu} --repo %{repo} --kmodname %{name} --filterfile %{SOURCE11} %{?buildforkernels:--%{buildforkernels}} %{?kernels:--for-kernels "%{?kernels}"} 2>/dev/null) }


%description
ThinkPad System Management API - kernel driver


%prep
# error out if there was something wrong with kmodtool
%{?kmodtool_check}
# print kmodtool output for debugging purposes:
kmodtool --target %{_target_cpu} --repo %{repo} --kmodname %{name} --filterfile %{SOURCE11} %{?buildforkernels:--%{buildforkernels}} %{?kernels:--for-kernels "%{?kernels}"} 2>/dev/null
%setup -q -c -T -a 0

for kernel_version in %{?kernel_versions}; do
    %{__cp} -rl tp_smapi-%{version} _kmod_source_${kernel_version%%___*}
done

%define base_module_list tp_smapi thinkpad_ec
%if %{with_hdaps}
%define hdaps_module hdaps
%else
%define hdaps_module %{nil}
%endif
%define module_list %{base_module_list} %{hdaps_module}


%build
for kernel_version in %{?kernel_versions}; do
    mkdir "_kmod_build_${kernel_version%%___*}"
    %{__make} -C "_kmod_source_${kernel_version%%___*}" \
               V=1 \
               O="_kmod_build_${kernel_version%%___*}" \
               KVER="${kernel_version%%___*}" \
               KSRC="${kernel_version##*___}" \
               KBUILD="${kernel_version##*___}" \
               HDAPS=%{with_hdaps} \
               modules
    for mod in %{module_list}; do
        mv -f "_kmod_source_${kernel_version%%___*}/${mod}.ko" "_kmod_build_${kernel_version%%___*}"
    done
    mv -f "_kmod_source_${kernel_version%%___*}/Module.symvers" "_kmod_build_${kernel_version%%___*}"
done


%install
%{__rm} -rf "%{buildroot}"
for kernel_version in %{?kernel_versions}; do
  for module in %{module_list}; do
    %{__install} -p -D -m 0755 \
        "_kmod_build_${kernel_version%%___*}/${module}.ko" \
        "%{buildroot}%{kmodinstdir_prefix}/${kernel_version%%___*}/%{kmodinstdir_postfix}/${module}.ko"
  done
done


%{?akmod_install}


%clean
%{__rm} -rf "%{buildroot}"


%changelog
* Mon Apr  6 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.40-3
- rebuild with current rpmfusion kmod toolset

* Wed Feb 18 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.40-2
- use %%{ix86} macro to determine "all 32bit x86 arches"

* Tue Feb 17 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.40-1
- update to tp_smapi-0.40
- add build requirement on akmods

* Wed Oct 08 2008 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.39-1
- update to tp_smapi-0.39

* Mon Apr 14 2008 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.37-2
- rebuild with new akmods-0.3.0-1.lvn9

* Sun Apr 13 2008 Hans Ulrich Niedermann <uli@nan.ltsp> - 0.37-1
- update to tp_smapi-0.37

* Sun Apr 13 2008 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.34-7
- remove tp_smapi-common package
- build hdaps module conditionally
- adapt tp_smapi-kmod to akmod system (based on iscsitarget-kmod.spec)

* Tue Mar 27 2008 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.32-6
- Rebuild for 2.6.24.3-50 kernel packages.

* Tue Mar 18 2008 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.32-5
- Rebuild for 2.6.24.3-34 kernel packages.

* Tue Feb 12 2008 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.32-4
- Rebuild for 2.6.24.3-12 kernel packages.

* Tue Feb 12 2008 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.32-3
- Use proper download URL for Source.
- Rebuild for 2.6.23.15-137 kernel packages.

* Thu Nov 29 2007 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.32-1
- Initial RPM.

